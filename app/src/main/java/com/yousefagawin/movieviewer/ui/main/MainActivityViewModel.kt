package com.yousefagawin.movieviewer.ui.main

import android.util.Log
import androidx.lifecycle.ViewModel
import com.yousefagawin.movieviewer.data.repository.MainRepository
import com.yousefagawin.movieviewer.util.Coroutines
import com.yousefagawin.movieviewer.util.NoInternetException
import org.json.JSONObject
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
private val mainRepository: MainRepository
): ViewModel(){

    var mainActivityListener: MainActivityListener? = null

    fun getMovie(){
        mainActivityListener?.onStarted()

        //the retrofit call runs on the main thread
        Coroutines.main {
            //this try-catch is used to check if there is no internet during the initial launch of the app
            try{

                val response = mainRepository.getMovie()

                if(response.isSuccessful){

                    val responseBody = response.body()

                    Log.e("content", responseBody.toString())

                    mainActivityListener!!.onSuccess(responseBody!!)

                }
                else{

                    val errorBody = response.errorBody()!!.string()
                    Log.e("errorBody", errorBody)
                    val errorMessage = JSONObject(errorBody).getString("message")

                    mainActivityListener?.onFailure("Error ${response.code()}: ${errorMessage}")
                }

            }catch (e: NoInternetException){
                mainActivityListener?.onFailure(e.message!!)
            }
        }
    }
}