package com.yousefagawin.movieviewer.ui.seatmap

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.core.view.setMargins
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.yousefagawin.movieviewer.R
import com.yousefagawin.movieviewer.datamodel.schedule.Cinema
import com.yousefagawin.movieviewer.datamodel.schedule.CinemaX
import com.yousefagawin.movieviewer.datamodel.schedule.Date
import com.yousefagawin.movieviewer.datamodel.schedule.ScheduleResponse
import com.yousefagawin.movieviewer.datamodel.seatmap.SeatMapResponse
import com.yousefagawin.movieviewer.util.hide
import com.yousefagawin.movieviewer.util.show
import com.yousefagawin.movieviewer.util.toast
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_second.*
import java.lang.Exception
import javax.inject.Inject
import android.view.ScaleGestureDetector
import android.text.method.Touch.onTouchEvent
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat.setScaleY
import androidx.core.view.ViewCompat.setScaleX
import android.widget.TextView




class SecondActivity : AppCompatActivity(), SecondActivityListener{

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SecondActivityViewModel

    private var ticketPrice: Long = 0
    private var totalTicketPrice: Long = 0
    private var ticketCounter: Int = 0

    var selectedSeatsMap:HashMap<String, TextView> = HashMap<String, TextView>()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)   //configure dagger
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SecondActivityViewModel::class.java)

        viewModel.secondActivityListener = this

        viewModel.getSched()
        viewModel.getSeatMap()


        val bundle = getIntent().getExtras()
        theater_tv.text = bundle!!.getString("theater")


        tryAgain_btn.setOnClickListener {
            viewModel.getSched()
            viewModel.getSeatMap()
        }

    }

    override fun onGetSchedStarted() {
        loading_pb1.show()
    }

    override fun onGetSchedSuccess(scheduleResponse: ScheduleResponse) {


        // Create an ArrayAdapter using a simple spinner layout and languages array
        val AADate = ArrayAdapter(this, R.layout.spinner_item_main, scheduleResponse.dates).apply {
            setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }
        // Set layout to use when the list of choices appear
        // Set Adapter to Spinner
        date_spn!!.setAdapter(AADate)

        date_spn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent0: AdapterView<*>?, view0: View?, position0: Int, id0: Long) {

//                toast((parent!!.selectedItem as Date).id)

                try{
                    val AACinema = ArrayAdapter(applicationContext,
                        R.layout.spinner_item_main,
                        scheduleResponse.cinemas[position0].cinemas)
                    AACinema.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    cinema_spn!!.setAdapter(AACinema)
                    cinema_spn.visibility = View.VISIBLE

                    cinema_spn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                        override fun onNothingSelected(p0: AdapterView<*>?) {

                        }

                        override fun onItemSelected(parent1: AdapterView<*>?, view1: View?, position1: Int, id1: Long) {

                            try{
                                val AATime = ArrayAdapter(applicationContext,
                                    R.layout.spinner_item_main,
                                    scheduleResponse.times[position1].times)

                                AATime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                time_spn!!.setAdapter(AATime)
                                time_spn.visibility = View.VISIBLE

                                time_spn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                                    override fun onNothingSelected(p0: AdapterView<*>?) {

                                    }

                                    override fun onItemSelected(parent2: AdapterView<*>?, view1: View?, position2: Int, id2: Long) {

                                        ticketPrice = scheduleResponse.times[position1].times[position2].price.toLong()
                                        computeTotalTicketPrice()
                                    }

                                }

                            } catch (e: Exception){
                                time_spn.visibility = View.INVISIBLE
                            }
                        }
                    }
                } catch (e: Exception){
                    cinema_spn.visibility = View.INVISIBLE
                    time_spn.visibility = View.INVISIBLE
                }

            }

        }

        loading_pb1.hide()
    }

    override fun onGetSchedFailure(message: String) {
        loading_pb1.hide()
        toast(message)

    }

    override fun onGetSeatStarted() {
        loading_pb2.show()
    }

    override fun onGetSeatSuccess(seatMapResponse: SeatMapResponse) {

        Log.e("iValue", seatMapResponse.seatmap.size.toString())
        Log.e("jValue", seatMapResponse.seatmap[1].size.toString())
        Log.e("m1", seatMapResponse.seatmap[12].get(0).toString())


        for (i in 0 .. seatMapResponse.seatmap.size -1) {
            val row = LinearLayout(this)
            row.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            //this is for text at the start
            var txtTag = TextView(this)
            var layoutParams = LinearLayout.LayoutParams( 0, LinearLayout.LayoutParams.WRAP_CONTENT ,1f)
            txtTag.setLayoutParams(layoutParams)

            txtTag.setText(seatMapResponse.seatmap[i][0].take(1))
            txtTag.setTextSize(8f)
            txtTag.gravity = Gravity.CENTER
            txtTag.setId((i+1)*10)
            row.addView(txtTag)

            for (j in 0 .. seatMapResponse.seatmap[i].size -1) {

                //this if statement will ensure that the seatmap dont add the blank areas
                if(!seatMapResponse.seatmap[i][j].contains("(")){

                    var btnTag = View(this)
                    var layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f)

                    //this if statement is used to give space if there is a blank on the next item
                    //this will also make sure that it will not crash on the last element
                    if((j+1) == (seatMapResponse.seatmap[i].size)){
                        layoutParams.setMargins(2,2,2,2)
                    }
                    else if(!seatMapResponse.seatmap[i][j+1].contains("(")){
                        layoutParams.setMargins(2,2,2,2)
                    }
                    else{
                        layoutParams.setMargins(2,2,9,2)
                    }
                    btnTag.setLayoutParams(layoutParams)

                    //this if statement will check if the seat is available or not
                    if(seatMapResponse.available.seats.contains(seatMapResponse.seatmap[i][j])){
                        btnTag.setBackgroundResource(R.drawable.available_icon)

                        btnTag.setOnClickListener {
                            //this if statement will check if the current btn is selected or not
                            val drawableAConstantState = ContextCompat.getDrawable(this, R.drawable.available_icon)?.constantState
                            if(it.background.constantState == drawableAConstantState){

                                if(ticketCounter < 10){
                                    btnTag.setBackgroundResource(R.drawable.selected_icon)

                                    ticketCounter ++
                                    computeTotalTicketPrice()


                                    addSeat(btnTag)
                                }
                                else{
                                    toast("You can only select maximum of 10 seats")
                                }

                            }
                            else{
                                btnTag.setBackgroundResource(R.drawable.available_icon)

                                deleteSeat(btnTag   )
                                ticketCounter--
                                computeTotalTicketPrice()
                            }
                        }
                    }
                    else{
                        btnTag.setBackgroundResource(R.drawable.reserved_icon)
                    }

                    btnTag.setTag(seatMapResponse.seatmap[i][j])
                    btnTag.setId(j + 1 + ((i+1)*10))
                    row.addView(btnTag)

                }

            }
            //this is for text at the end
            var txtTag2 = TextView(this)
            var layoutParams2 = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
            txtTag2.setLayoutParams(layoutParams2)
            txtTag2.setText(seatMapResponse.seatmap[i][0].take(1))
            txtTag2.setTextSize(8f)
            txtTag2.gravity = Gravity.CENTER
            txtTag2.setId((seatMapResponse.seatmap[i].size + 1) + ((i+1)*10))
            row.addView(txtTag2)

            seatMap_container.addView(row)
        }

        seatMap_container as ZoomableViewGroup


        error_layout.visibility = View.INVISIBLE
        content_layout.visibility = View.VISIBLE
        loading_pb2.hide()
    }

    override fun onGetSeatFailure(message: String) {
        loading_pb2.hide()

        error_layout.visibility = View.VISIBLE
        error_tv.text = message
    }


    private fun computeTotalTicketPrice(){

        totalTicketPrice = ticketPrice* ticketCounter

        val stringTotalTicketPrice = String.format("%,d", totalTicketPrice)
        total_tv.text = "Php " + stringTotalTicketPrice + ".00"

    }

    private fun addSeat(view: View?){

        Log.e("onclick", "entered")
        //this is for text at the start
        var seatTv = TextView(this)
        var layoutParams = LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(4,2,4,2)
        seatTv.setLayoutParams(layoutParams)

        seatTv.setText(view!!.tag.toString())
        seatTv.setTextSize(14f)
        seatTv.setTextColor(resources.getColor(R.color.white100))
        seatTv.setBackgroundColor(resources.getColor(R.color.colorAccent))
        seatTv.gravity = Gravity.CENTER
        seatTv.setPadding(4,0,4,0)
        selectedSeats_layout.addView(seatTv)


        selectedSeatsMap.put(view.tag.toString(), seatTv)
    }

    private fun deleteSeat(view: View?){

        selectedSeats_layout.removeView(selectedSeatsMap.get(view!!.tag.toString()))
        selectedSeatsMap.remove(view.tag.toString())
    }
}
