package com.yousefagawin.movieviewer.ui.seatmap

import com.yousefagawin.movieviewer.datamodel.schedule.ScheduleResponse
import com.yousefagawin.movieviewer.datamodel.seatmap.SeatMapResponse

interface SecondActivityListener {

    fun onGetSchedStarted()
    fun onGetSchedSuccess(scheduleResponse: ScheduleResponse)
    fun onGetSchedFailure(message: String)

    fun onGetSeatStarted()
    fun onGetSeatSuccess(seatMapResponse: SeatMapResponse)
    fun onGetSeatFailure(message: String)
}