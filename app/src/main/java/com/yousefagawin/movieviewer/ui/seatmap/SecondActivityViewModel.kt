package com.yousefagawin.movieviewer.ui.seatmap

import android.util.Log
import androidx.lifecycle.ViewModel
import com.yousefagawin.movieviewer.data.repository.MainRepository
import com.yousefagawin.movieviewer.util.Coroutines
import com.yousefagawin.movieviewer.util.NoInternetException
import org.json.JSONObject
import javax.inject.Inject

class SecondActivityViewModel @Inject constructor(
    private val mainRepository: MainRepository
): ViewModel(){

    var secondActivityListener: SecondActivityListener? = null

    fun getSched(){
        secondActivityListener?.onGetSchedStarted()

        //the retrofit call runs on the main thread
        Coroutines.main {
            //this try-catch is used to check if there is no internet during the initial launch of the app
            try{

                val response = mainRepository.getSchedule()

                if(response.isSuccessful){

                    val responseBody = response.body()

                    Log.e("content", responseBody.toString())

                    secondActivityListener!!.onGetSchedSuccess(responseBody!!)

                }
                else{

                    val errorBody = response.errorBody()!!.string()
                    Log.e("errorBody", errorBody)
                    val errorMessage = JSONObject(errorBody).getString("message")

                    secondActivityListener?.onGetSchedFailure("Error ${response.code()}: ${errorMessage}")
                }

            }catch (e: NoInternetException){
                secondActivityListener?.onGetSchedFailure(e.message!!)
            }
        }
    }

    fun getSeatMap(){
        secondActivityListener?.onGetSeatStarted()

        //the retrofit call runs on the main thread
        Coroutines.main {
            //this try-catch is used to check if there is no internet during the initial launch of the app
            try{

                val response = mainRepository.getSeatMap()

                if(response.isSuccessful){

                    val responseBody = response.body()

                    Log.e("content", responseBody.toString())

                    secondActivityListener!!.onGetSeatSuccess(responseBody!!)

                }
                else{

                    val errorBody = response.errorBody()!!.string()
                    Log.e("errorBody", errorBody)
                    val errorMessage = JSONObject(errorBody).getString("message")

                    secondActivityListener?.onGetSeatFailure("Error ${response.code()}: ${errorMessage}")
                }

            }catch (e: NoInternetException){
                secondActivityListener?.onGetSeatFailure(e.message!!)
            }
        }
    }
}