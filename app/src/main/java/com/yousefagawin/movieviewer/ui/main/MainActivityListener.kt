package com.yousefagawin.movieviewer.ui.main

import com.yousefagawin.movieviewer.datamodel.movie.MovieResponse

interface MainActivityListener {

    fun onStarted()
    fun onSuccess(getMovieResponse: MovieResponse)
    fun onFailure(message: String)
}