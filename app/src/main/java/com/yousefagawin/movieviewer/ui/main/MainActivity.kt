package com.yousefagawin.movieviewer.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.yousefagawin.movieviewer.R
import com.yousefagawin.movieviewer.datamodel.movie.MovieResponse
import com.yousefagawin.movieviewer.util.hide
import com.yousefagawin.movieviewer.util.show
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.inject.Inject
import android.icu.text.SimpleDateFormat
import android.view.View
import com.yousefagawin.movieviewer.ui.seatmap.SecondActivity
import com.yousefagawin.movieviewer.util.toast


class MainActivity : AppCompatActivity(), MainActivityListener {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)   //configure dagger
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)

        viewModel.mainActivityListener = this


        viewModel.getMovie()


        tryAgain_btn.setOnClickListener {

            viewModel.getMovie()
        }
    }


    override fun onStarted() {

        loading_pb.show()
    }

    override fun onSuccess(getMovieResponse: MovieResponse) {

        Glide.with(this).load(getMovieResponse.posterLandscape)
            //this will handle the issue if image is not loaded
            .into(posterLandscape_iv)
        Glide.with(this).load(getMovieResponse.poster)
            //this will handle the issue if image is not loaded
            .into(poster_iv)

        name_tv.text = getMovieResponse.canonicalTitle
        genre_tv.text = getMovieResponse.genre
        advisory_tv.text = getMovieResponse.advisoryRating

        val runtimeMins = getMovieResponse.runtimeMins.toFloat().toLong()
        val minutes = runtimeMins % 60
        val hours = runtimeMins / 60
        if(minutes == 0L){
            duration_tv.text = hours.toString() + " hr"
        }
        else{
            duration_tv.text = hours.toString() + " hr & "  + minutes.toString() + " min"
        }

        val inputFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputFormat = SimpleDateFormat("MMM dd, yyyy")
        val date = inputFormat.parse(getMovieResponse.releaseDate)
        val outputDateStr = outputFormat.format(date)

        release_tv.text = outputDateStr
        synopsis_tv.text = getMovieResponse.synopsis

        viewSeatMap_btn.setOnClickListener {
            val intent = Intent (this@MainActivity, SecondActivity::class.java)
            intent.putExtra("theater", getMovieResponse.theater)
            this@MainActivity.startActivity(intent)
        }

        var castsString = ""
        getMovieResponse.cast.forEachIndexed { index, string ->

            castsString += string

            if(!(index == (getMovieResponse.cast.size-1))){
                castsString += ", "
            }
        }
        casts_tv.text = castsString

        loading_pb.hide()
        error_layout.visibility = View.INVISIBLE
        content_layout.visibility = View.VISIBLE
        viewSeatMap_btn.visibility = View.VISIBLE
    }

    override fun onFailure(message: String) {
        error_layout.visibility = View.VISIBLE
        error_tv.text = message

        loading_pb.hide()
    }

}
