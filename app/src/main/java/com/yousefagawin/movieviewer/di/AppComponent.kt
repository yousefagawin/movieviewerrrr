package com.yousefagawin.movieviewer.di

import android.app.Application
import com.yousefagawin.movieviewer.App
import com.yousefagawin.movieviewer.di.module.AppModule
import com.yousefagawin.movieviewer.di.module.main.MainActivityModule
import com.yousefagawin.movieviewer.di.module.second.SecondActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    //put here the AppModule and the fragments, services & activity modules at com.di.modules
    AndroidSupportInjectionModule::class,
    AppModule::class,
    MainActivityModule::class,
    SecondActivityModule::class

])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}