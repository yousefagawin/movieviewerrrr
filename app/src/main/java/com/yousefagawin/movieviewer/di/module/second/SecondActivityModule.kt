package com.yousefagawin.movieviewer.di.module.second

import com.yousefagawin.movieviewer.ui.seatmap.SecondActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

//declare fragments, service, & activities like this
@Module
abstract class SecondActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeSecondActivity(): SecondActivity
}