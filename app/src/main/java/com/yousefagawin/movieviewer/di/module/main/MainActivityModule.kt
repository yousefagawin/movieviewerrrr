package com.yousefagawin.movieviewer.di.module.main

import com.yousefagawin.movieviewer.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

//declare fragments, service, & activities like this
@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}