package com.yousefagawin.movieviewer.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yousefagawin.movieviewer.di.FactoryViewModel
import com.yousefagawin.movieviewer.di.ViewModelKey
import com.yousefagawin.movieviewer.ui.main.MainActivityViewModel
import com.yousefagawin.movieviewer.ui.seatmap.SecondActivityViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    //put here all the ViewModels of fragments & activities
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainActivityViewModel(viewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SecondActivityViewModel::class)
    abstract fun bindSecondActivityViewModel(viewModel: SecondActivityViewModel): ViewModel


    @Binds
    abstract fun bindViewModelFactory(factory: FactoryViewModel): ViewModelProvider.Factory
}