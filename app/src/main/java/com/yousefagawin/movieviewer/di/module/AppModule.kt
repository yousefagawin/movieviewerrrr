package com.yousefagawin.movieviewer.di.module

import android.app.Application
import com.yousefagawin.movieviewer.data.network.MyApiInterface
import com.yousefagawin.movieviewer.data.network.NetworkConnectionInterceptor
import com.yousefagawin.movieviewer.data.repository.MainRepository
import dagger.Module
import dagger.Provides

@Module(includes = [
    ViewModelModule::class
])
class AppModule {

    @Provides
    internal fun provideNetworkConnectionInterceptor(application: Application): NetworkConnectionInterceptor{
        return NetworkConnectionInterceptor(application)
    }

    @Provides
    internal fun provideMyApiInterface(networkConnectionInterceptor: NetworkConnectionInterceptor): MyApiInterface {
        return MyApiInterface(networkConnectionInterceptor)
    }


    @Provides
    internal fun provideMainRepository(myApiInterface: MyApiInterface) : MainRepository {
        return MainRepository(myApiInterface)
    }

}