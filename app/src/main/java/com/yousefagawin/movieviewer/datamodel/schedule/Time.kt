package com.yousefagawin.movieviewer.datamodel.schedule


import com.google.gson.annotations.SerializedName

data class Time(
    val parent: String,
    val times: List<TimeX>
){
    override fun toString(): String {
        return super.toString()
    }
}