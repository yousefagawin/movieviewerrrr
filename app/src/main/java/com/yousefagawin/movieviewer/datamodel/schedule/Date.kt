package com.yousefagawin.movieviewer.datamodel.schedule


import com.google.gson.annotations.SerializedName

data class Date(
    val date: String,
    val id: String,
    val label: String
){
    override fun toString(): String {
        return label
    }
}