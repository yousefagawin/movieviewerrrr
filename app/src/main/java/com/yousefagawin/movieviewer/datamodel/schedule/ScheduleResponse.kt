package com.yousefagawin.movieviewer.datamodel.schedule


import com.google.gson.annotations.SerializedName

data class ScheduleResponse(
    val cinemas: List<Cinema>,
    val dates: List<Date>,
    val times: List<Time>
)