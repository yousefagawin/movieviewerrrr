package com.yousefagawin.movieviewer.datamodel.schedule


import com.google.gson.annotations.SerializedName

data class CinemaX(
    @SerializedName("cinema_id")
    val cinemaId: String,
    val id: String,
    val label: String
){
    override fun toString(): String {
        return label
    }
}