package com.yousefagawin.movieviewer.datamodel.schedule


import com.google.gson.annotations.SerializedName

data class TimeX(
    val id: String,
    val label: String,
    @SerializedName("popcorn_label")
    val popcornLabel: String,
    @SerializedName("popcorn_price")
    val popcornPrice: String,
    val price: String,
    @SerializedName("schedule_id")
    val scheduleId: String,
    @SerializedName("seating_type")
    val seatingType: String,
    val variant: Any
){
    override fun toString(): String {
        return label
    }
}