package com.yousefagawin.movieviewer.datamodel.seatmap


import com.google.gson.annotations.SerializedName

data class SeatMapResponse(
    val available: Available,
    val seatmap: List<List<String>>
)