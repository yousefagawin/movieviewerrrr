package com.yousefagawin.movieviewer.datamodel.schedule


import com.google.gson.annotations.SerializedName

data class Cinema(
    val cinemas: List<CinemaX>,
    val parent: String
)