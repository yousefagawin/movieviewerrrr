package com.yousefagawin.movieviewer.datamodel.seatmap


import com.google.gson.annotations.SerializedName

data class Available(
    @SerializedName("seat_count")
    val seatCount: Int,
    val seats: List<String>
)