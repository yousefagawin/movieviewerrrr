package com.yousefagawin.movieviewer.data.repository

import com.yousefagawin.movieviewer.data.network.MyApiInterface
import com.yousefagawin.movieviewer.datamodel.movie.MovieResponse
import com.yousefagawin.movieviewer.datamodel.schedule.ScheduleResponse
import com.yousefagawin.movieviewer.datamodel.seatmap.SeatMapResponse
import retrofit2.Response

class MainRepository (
    private val api: MyApiInterface
){
    //this function will now return an MovieResponse directly because of the SafeApiRequest
    suspend fun getMovie(): Response<MovieResponse> {

        return api.getMovie()
    }

    suspend fun getSchedule(): Response<ScheduleResponse> {

        return api.getSchedule()
    }

    suspend fun getSeatMap(): Response<SeatMapResponse> {

        return api.getSeatMap()
    }

//    //this function will return the songs from the database
//    fun getAllSongs() = db.songDao().getAll()
//    suspend fun saveSongs(songEntity: SongEntity) = db.songDao().insertAll(songEntity)

}